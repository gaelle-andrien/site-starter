const isIE = (ua: string): boolean => {
  if (ua) {
    const msie = ua.indexOf("MSIE "); // IE 10 or older => return version number
    const trident = ua.indexOf("Trident/"); // IE 11 => return version number

    if (msie > 0 || trident > 0) return true;
  }

  return false;
};

export default isIE;
