export enum Position {
  Top = "TOP",
  Bottom = "BOTTOM",
  Left = "LEFT",
  Right = "RIGHT",
}

export enum Align {
  Center = "CENTER",
  Left = "LEFT",
  Right = "RIGHT",
}
