import { useTranslation } from "next-i18next";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { GetStaticProps } from "next";
import Section from "../../components/Section";
import Container from "../../components/Container";
import SectionText from "../../components/SectionText";
import SectionTitle from "../../components/SectionTitle";

import { wrapParagraph } from "../../utils/wrapContent";

const PresentationSection = (): React.ReactElement => {
  const section = "sectionAnchor";
  const { t } = useTranslation();

  return (
    <Section id={section}>
      <Container>
        <SectionTitle title={t(`home:${section}.title`)} />
        <SectionText size={100}>
          {wrapParagraph(t(`home:${section}.text`, { returnObjects: true }))}
        </SectionText>
      </Container>
    </Section>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "home"])),
  },
});

export default PresentationSection;
