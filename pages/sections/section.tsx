import { useTranslation } from "next-i18next";

import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { GetStaticProps } from "next";
import Button from "../../components/Button";
import Section from "../../components/Section";
import Container from "../../components/Container";
import SectionText from "../../components/SectionText";
import SectionTitle from "../../components/SectionTitle";

import { Position } from "../../utils/types";
import { wrapParagraph } from "../../utils/wrapContent";

const PresentationSection = (): React.ReactElement => {
  const section = "section";
  const { t } = useTranslation();

  return (
    <Section id={section}>
      <Container>
        <SectionTitle title={t(`home:section.title`)} />
        <SectionText size={100}>
          {wrapParagraph(t(`home:${section}.text`, { returnObjects: true }))}
        </SectionText>
        <br />
        <h3>Link</h3>
        <a
          href="https://www.linkedin.com/in/gaelle-andrien-13292929/"
          target="_blank"
          rel="noreferrer"
        >
          Link to linkedin
        </a>
        <br />
        <h3>Button</h3>
        <Button title="[ Button ]" />
        <Button
          title="[ Button left side icon ]"
          icon="arrow_back"
          iconSide={Position.Left}
        />
        <Button
          title="[ Button right side icon ]"
          icon="arrow_forward"
          iconSide={Position.Right}
        />
      </Container>
    </Section>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "home"])),
  },
});

export default PresentationSection;
