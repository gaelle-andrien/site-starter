import { appWithTranslation } from "next-i18next";

import globalStyles from "../assets/styles/global";

import Error from "./_error";

import isIE from "../utils/isIE";

const MyApp = ({ Component, pageProps }) => {
  let isUserAgentIE = false;
  const { errorCode } = pageProps;

  if (!(typeof window === "undefined")) {
    isUserAgentIE = isIE(window.navigator.userAgent);
  }

  return (
    <>
      {globalStyles}
      {isUserAgentIE || errorCode ? (
        <Error statusCode={errorCode} isIE={isUserAgentIE} />
      ) : (
        <Component {...pageProps} />
      )}
    </>
  );
};

export default appWithTranslation(MyApp);
