import { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Head from "../components/Head";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Container from "../components/Container";
import ScrollTop from "../components/ScrollTop";

import Section from "./sections/section";
import SectionAnchor from "./sections/sectionAnchor";

const Home = (): React.ReactElement => {
  const { t } = useTranslation();

  return (
    <>
      <Head
        pageURL={t("home:url")}
        pageTitle={t("home:title")}
        pageDescription={t("home:description")}
      />

      <Header title={t("home:title")} />

      <main>
        <Container>
          <h1>{t("home:title")}</h1>
        </Container>

        <Section />
        <SectionAnchor />
      </main>

      <ScrollTop />
      <Footer />
    </>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) =>
  // const res = await fetch("https://api.github.com/repos/vercel/next.js");
  // const errorCode = res.ok ? false : res.status;
  // const json = await res.json();

  ({
    props: {
      // errorCode,
      ...(await serverSideTranslations(locale, ["common", "home"])),
    },
  });

export default Home;
