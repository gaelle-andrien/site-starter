import { GetStaticProps } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

import Head from "../components/Head";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Container from "../components/Container";
import ScrollTop from "../components/ScrollTop";

const Contact = (): React.ReactElement => {
  const { t } = useTranslation();

  return (
    <>
      <Head
        pageURL={t("contact:url")}
        pageTitle={t("contact:title")}
        pageDescription={t("contact:description")}
      />

      <Header title={t("contact:title")} />

      <main>
        <Container>
          <h1>{t("contact:title")}</h1>
          <p>{t("contact:description")}</p>
        </Container>
      </main>

      <ScrollTop />
      <Footer />
    </>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale }) => ({
  props: {
    ...(await serverSideTranslations(locale, ["common", "contact"])),
  },
});

export default Contact;
