import { Global } from "@emotion/react";

import { mq } from "./mediaqueries";
import { defaultFont } from "./fonts";
import { defaultTextColor, bodyBackgroundColor, brandColorRed } from "./colors";

export const layoutMaxWidth = 1024;

const globalStyles = (
  <Global
    styles={{
      "*": {
        boxSizing: "border-box",
      },

      "html, body": {
        margin: 0,
        padding: 0,
      },

      body: {
        fontSize: 12,
        height: "100vh",
        fontWeight: 300,
        color: defaultTextColor,
        fontFamily: defaultFont,
        background: bodyBackgroundColor,

        [mq[1]]: {
          fontSize: 13,
        },

        [mq[2]]: {
          fontSize: 14,
        },

        [mq[3]]: {
          fontSize: 15,
        },
      },

      "#__next": {
        display: "flex",
        minHeight: "100%",
        flexDirection: "column",
        justifyContent: "center",
      },

      main: {
        flex: 1,
        width: "100%",
        paddingTop: 50, // Header height
        overflow: "hidden",
        alignSelf: "center",

        [mq[2]]: {
          paddingTop: 70, // Header height
        },
      },

      p: {
        marginTop: 0,
        lineHeight: "1.8em",
      },

      ul: {
        padding: 0,
        paddingBottom: 15,
        lineHeight: "1.5em",
      },

      li: {
        listStyle: "none",
      },

      button: {
        border: 0,
        fontSize: "inherit",
        fontFamily: "inherit",
        backgroundColor: "transparent",

        "&:focus": {
          outline: 0,
        },
      },

      img: {
        maxWidth: "100%",
      },

      a: {
        fontWeight: 500,
        color: brandColorRed,
        textDecoration: "none",

        "&:after": {
          height: 2,
          bottom: 0,
          width: "100%",
          content: '" "',
          display: "block",
          backgroundColor: brandColorRed,
        },

        "&[href^='tel']": {
          color: "inherit",
          textDecoration: "none",
        },
      },

      h1: {
        fontSize: 35,
        paddingTop: 30,
        fontWeight: 700,
        letterSpacing: 2,

        [mq[1]]: {
          fontSize: 40,
        },

        [mq[2]]: {
          fontSize: 45,
          paddingTop: 50,
        },

        [mq[3]]: {
          fontSize: 50,
        },
      },

      h2: {
        fontSize: 18,
        fontWeight: 900,
        letterSpacing: 2,

        [mq[1]]: {
          fontSize: 20,
        },

        [mq[2]]: {
          fontSize: 25,
        },

        [mq[3]]: {
          fontSize: 30,
        },
      },
    }}
  />
);

export default globalStyles;
