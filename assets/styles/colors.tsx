export const brandColor = "#4D596F";
export const brandColorBlue = "#212E47";
export const brandColorGreen = "#ABF9A8";
export const brandColorRed = "#EF767A";

export const defaultTextColor = "dark grey";
export const defaultIconColor = brandColorBlue;

export const bodyBackgroundColor = "white";
export const footerBackgroundColor = "white";
