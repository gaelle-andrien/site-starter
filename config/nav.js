const nav = [
  {
    name: "home",
    path: "/",
    sections: [
      {
        name: "sectionAnchor",
        anchor: "sectionAnchor",
      },
    ],
  },
  {
    name: "contact",
    path: "/contact",
  },
];

export default nav;
