/** @jsxImportSource @emotion/react */

import StyledContainer from "./styles";

interface IContainerProps {
  hasColumns?: boolean;
  children: React.ReactNode;
  isVerticallyCentered?: boolean;
}

const Container = ({
  children,
  hasColumns = false,
  isVerticallyCentered = false,
}: IContainerProps): React.ReactElement => (
  <StyledContainer
    hasColumns={hasColumns}
    isVerticallyCentered={isVerticallyCentered}
  >
    {children}
  </StyledContainer>
);

export default Container;
