import styled from "@emotion/styled";

import { mq } from "../../assets/styles/mediaqueries";
import { layoutMaxWidth } from "../../assets/styles/global";

interface IStyledContainer {
  hasColumns: boolean;
  isVerticallyCentered: boolean;
}

const StyledContainer = styled.div<IStyledContainer>(
  ({ hasColumns, isVerticallyCentered }) => ({
    width: "100%",
    display: "flex",
    margin: "0 auto",
    position: "relative",
    flexDirection: "column",
    maxWidth: layoutMaxWidth,
    alignItems: "flex-start",
    justifyContent: isVerticallyCentered && "center",

    [mq[1]]: {
      alignItems: hasColumns && "center",
      flexDirection: hasColumns ? "row" : "column",
    },
  })
);

export default StyledContainer;
