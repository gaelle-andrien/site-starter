import { mq } from "../../assets/styles/mediaqueries";
import { brandColorGreen } from "../../assets/styles/colors";

export const languageSelectorStyle = {
  margin: 0,
  padding: 0,
  display: "flex",
};

export const languageSelectorButtonStyle = {
  margin: 0,
  padding: 0,
  paddingTop: 5,
  marginLeft: 5,
  marginRight: 5,
  color: "white",
  fontWeight: 700,
  paddingBottom: 5,
  letterSpacing: 2,
  cursor: "pointer",
  position: "relative" as const,
  transition: "all .2s ease-in-out",
  textTransform: "uppercase" as const,

  "&::after": {
    height: 2,
    bottom: 2,
    width: "100%",
    content: '" "',
    display: "block",
    position: "absolute" as const,
    backgroundColor: "transparent",
    transition: "all .5s ease-in-out",
  },

  "&:hover": {
    color: brandColorGreen,
  },

  "&:disabled": {
    cursor: "default",
    color: brandColorGreen,

    "&::after": {
      backgroundColor: brandColorGreen,
    },
  },

  [mq[2]]: {
    fontSize: 11,
    paddingTop: 10,
    paddingBottom: 10,
  },

  [mq[3]]: {
    fontSize: 12,
  },
};
