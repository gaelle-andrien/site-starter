/** @jsxImportSource @emotion/react */
import { useRouter } from "next/router";
import { useTranslation } from "next-i18next";

import { languageSelectorStyle, languageSelectorButtonStyle } from "./styles";

const LanguageSelector = (): React.ReactElement => {
  const router = useRouter();
  const { locales, asPath } = router;
  const { t, i18n } = useTranslation();

  const onLanguageChange = (lang: string) => {
    i18n.changeLanguage(lang);
    router.push(asPath, undefined, { locale: lang });
  };

  return (
    <ul css={languageSelectorStyle}>
      {locales.map((lang) => (
        <li key={lang}>
          <button
            type="button"
            css={languageSelectorButtonStyle}
            disabled={i18n.language === lang}
            onClick={() => onLanguageChange(lang)}
          >
            {t(`languageSelector.${lang}`)}
          </button>
        </li>
      ))}
    </ul>
  );
};

export default LanguageSelector;
