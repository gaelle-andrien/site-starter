/** @jsxImportSource @emotion/react */

import iconPath from "../../assets/icons/icons";

import { iconStyle, StyledIconPath } from "./styles";

interface IIconProps {
  icon: string;
  size?: number;
  color?: string;
  viewBox?: string;
}

const Icon = ({
  icon,
  color,
  size = 24,
  viewBox = "0 0 24 24",
}: IIconProps): React.ReactElement => (
  <svg
    css={iconStyle}
    viewBox={viewBox}
    width={`${size}px`}
    height={`${size}px`}
  >
    <StyledIconPath fill={color} d={iconPath[icon]} />
  </svg>
);

export default Icon;
