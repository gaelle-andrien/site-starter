import Head from "next/head";

interface ICustomHeadProps {
  pageURL: string;
  pageTitle: string;
  pageDescription: string;
}

function CustomHead({
  pageURL,
  pageTitle,
  pageDescription,
}: ICustomHeadProps): React.ReactElement {
  return (
    <Head>
      <title>{pageTitle}</title>
      <meta name="description" content={pageDescription} />
      <link rel="icon" href="/favicon.ico" />

      <meta property="og:type" content="website" />
      <meta property="og:title" content={pageTitle} />
      <meta property="og:description" content={pageDescription} />
      <meta property="og:url" content={pageURL} />
      <meta property="og:image" content={`${pageURL}/static/og-logo.jpg`} />
    </Head>
  );
}

export default CustomHead;
