import { mq } from "../../assets/styles/mediaqueries";
import { brandColorBlue } from "../../assets/styles/colors";

export const headerStyle = {
  top: 0,
  zIndex: 15,
  padding: 10,
  width: "100%",
  display: "flex",
  paddingLeft: 15,
  paddingRight: 15,
  alignSelf: "center",
  flexWrap: "wrap" as const,
  position: "fixed" as const,
  flexDirection: "row" as const,
  justifyContent: "space-between",
  backgroundColor: brandColorBlue,

  [mq[1]]: {
    paddingLeft: 20,
    paddingRight: 20,
  },

  [mq[2]]: {
    padding: 15,
    paddingLeft: 25,
    paddingRight: 25,
    alignItems: "center",
    flexWrap: "nowrap" as const,
  },

  [mq[3]]: {
    paddingLeft: 30,
    paddingRight: 30,
  },
};

export const headerNavStyle = {
  top: 50,
  left: 0,
  right: 0,
  padding: 15,
  paddingTop: 0,
  width: "100%",
  display: "none",
  overflow: "hidden",
  position: "absolute" as const,
  backgroundColor: brandColorBlue,
  flexDirection: "column" as const,

  [mq[2]]: {
    padding: 0,
    top: "auto",
    display: "flex",
    alignItems: "center",
    flexDirection: "row" as const,
    position: "relative" as const,
    justifyContent: "space-between",
  },
};
