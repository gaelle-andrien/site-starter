const variantsHeader = {
  open: {
    y: 0,
    opacity: 1,
    transition: {
      duration: 0.3,
      mass: 0.8,
      type: "spring",
    },
    display: "flex",
  },
  closed: {
    y: 1000,
    transition: {
      duration: 0.3,
    },
    transitionEnd: {
      y: -100,
      display: "none",
    },
  },
};

export default variantsHeader;
