import styled from "@emotion/styled";
import { mq } from "../../../../assets/styles/mediaqueries";
import {
  brandColorBlue,
  brandColorGreen,
} from "../../../../assets/styles/colors";

interface IStyledNavItem {
  isActive: boolean;
}

export const StyledNavItem = styled.li<IStyledNavItem>(({ isActive }) => ({
  paddingTop: 5,
  paddingBottom: 5,
  marginBottom: 15,
  position: "relative",
  cursor: isActive ? "default" : "pointer",

  "&::after": {
    height: 2,
    bottom: 0,
    width: "100%",
    content: '" "',
    display: "block",
    position: "absolute",
    transition: "all .5s ease-in-out",
    backgroundColor: isActive ? brandColorGreen : brandColorBlue,
  },

  [mq[2]]: {
    paddingTop: 10,
    marginBottom: 0,
    paddingBottom: 10,

    "&::after": {
      bottom: 2,
      marginLeft: 15,
      marginRight: 15,
      width: "calc(100% - 30px)",
    },
  },

  [mq[3]]: {
    "&::after": {
      marginLeft: 20,
      marginRight: 20,
      width: "calc(100% - 40px)",
    },
  },
}));

export const navItemButtonStyle = {
  border: 0,
  padding: 0,
  color: "white",
  fontWeight: 500,
  marginBottom: 5,
  display: "block",
  cursor: "inherit",
  fontSize: "1.1em",
  letterSpacing: 0.5,
  transition: "all .2s ease-in-out",

  "&:hover": {
    color: brandColorGreen,
  },

  [mq[2]]: {
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 0,
    fontSize: "0.9em",
  },

  [mq[3]]: {
    paddingLeft: 20,
    paddingRight: 20,
  },
};
