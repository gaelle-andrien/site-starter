/** @jsxImportSource @emotion/react */
import Link from "next/link";
import { useRouter } from "next/router";

import { useState, useEffect } from "react";
import { useTranslation } from "next-i18next";

import { StyledNavItem, navItemButtonStyle } from "./styles";

interface INavItemProps {
  name: string;
  path: string;
  anchorId?: string;
  isActive: boolean;
  onClick: () => void;
}

const NavItem = ({
  name,
  path,
  onClick,
  isActive,
  anchorId = null,
}: INavItemProps): React.ReactElement => {
  const router = useRouter();
  const { t } = useTranslation();
  const { pathname: routerPath } = useRouter();
  const [anchorTarget, setAnchorTarget] = useState(null);

  const handleAnchorTarget = (Id: string) => document.getElementById(Id);

  const scrollToAnchor = (anchor: HTMLElement) => {
    anchor.scrollIntoView({ behavior: "smooth", block: "start" });
  };

  const onClickAnchor = (event: React.MouseEvent) => {
    event.preventDefault();
    onClick();

    if (path === routerPath) {
      scrollToAnchor(anchorTarget);
    } else {
      router.push(path).then(() => {
        scrollToAnchor(handleAnchorTarget(anchorId));
      });
    }
  };

  useEffect(() => {
    if (anchorId && path === routerPath)
      setAnchorTarget(handleAnchorTarget(anchorId));
  });

  return (
    <StyledNavItem isActive={isActive}>
      <Link href={path}>
        <button
          type="button"
          css={navItemButtonStyle}
          onClick={anchorId && onClickAnchor}
        >
          {t(`nav.${name}`)}
        </button>
      </Link>
    </StyledNavItem>
  );
};

export default NavItem;
