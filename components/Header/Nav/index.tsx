/** @jsxImportSource @emotion/react */
import { useRouter } from "next/router";
import { useState, useEffect } from "react";

import NavItem from "./NavItem";

import nav from "../../../config/nav";

import navStyle from "./styles";

interface IAnchor {
  name: string;
  anchor: string;
}

interface IPage {
  name: string;
  path: string;
  sections?: IAnchor[];
}

interface INavProps {
  onClick: () => void;
}

const Nav = ({ onClick }: INavProps): React.ReactElement => {
  let anchorPoints;
  const { pathname } = useRouter();
  const [activeItem, setActiveItem] = useState(null);
  const [isScrollNav, setScrollNav] = useState(false);

  const getCurrentPage = (): IPage =>
    nav.find((navItem) => navItem.path === pathname);

  const getAnchorPoints = () => {
    const offset = 5;
    const curScroll = window.scrollY - offset;

    return nav
      .map((navItem) => {
        if (navItem.sections && navItem.path === pathname) {
          const anchorPoints2 = navItem.sections.map(({ name, anchor }) => {
            const position = Math.round(
              document.getElementById(anchor).getBoundingClientRect().top +
                curScroll
            );
            return {
              name,
              position,
            };
          });

          return anchorPoints2;
        }
        return null;
      })
      .filter((el) => el !== undefined)[0];
  };

  const handleActiveAnchor = (): void => {
    let currentSection;
    const currentPosition = window.scrollY;

    anchorPoints.map(({ name: anchorName, position: anchorPosition }) => {
      currentSection =
        anchorPosition <= currentPosition ? anchorName : currentSection;

      return currentSection;
    });

    if (currentSection !== activeItem) {
      setActiveItem(currentSection);
    }
  };

  const getActiveNavItem = async () => {
    const { name, sections } = getCurrentPage();

    if (sections) {
      setScrollNav(true);
      anchorPoints = await getAnchorPoints();
      handleActiveAnchor();
      return;
    }

    setActiveItem(name);
  };

  useEffect(() => {
    getActiveNavItem();

    if (isScrollNav) {
      window.addEventListener("scroll", handleActiveAnchor);
    }
    return () => {
      window.removeEventListener("scroll", handleActiveAnchor);
    };
  }, [activeItem]);

  return (
    <ul css={navStyle}>
      {nav.map(({ name, path, sections }) => {
        if (sections) {
          return sections.map(({ name: section, anchor }) => (
            <NavItem
              key={section}
              path={path}
              anchorId={anchor}
              name={section}
              onClick={onClick}
              isActive={section === activeItem}
            />
          ));
        }

        return (
          <NavItem
            key={name}
            path={path}
            name={name}
            onClick={onClick}
            isActive={name === activeItem}
          />
        );
      })}
    </ul>
  );
};

export default Nav;
