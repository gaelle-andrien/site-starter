import { mq } from "../../../assets/styles/mediaqueries";
import { brandColorBlue } from "../../../assets/styles/colors";

const navStyle = {
  margin: 0,
  padding: 0,
  paddingTop: 15,
  display: "flex",
  flexDirection: "column" as const,
  borderTop: `1px solid ${brandColorBlue}02`,

  [mq[2]]: {
    borderTop: 0,
    paddingTop: 0,
    paddingLeft: 30,
    flexDirection: "row" as const,
  },
};

export default navStyle;
