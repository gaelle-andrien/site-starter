/** @jsxImportSource @emotion/react */
import { useState, useEffect } from "react";
import { motion, useCycle } from "framer-motion";

import Nav from "./Nav";
import Logo from "../Logo";
import ToggleNav from "./ToggleNav";
import LanguageSelector from "../LanguageSelector";

import { isTablet } from "../../utils/windowWidth";

import variantsHeader from "./variants";
import { headerStyle, headerNavStyle } from "./styles";

interface IHeaderProps {
  title: string;
}

const Header = ({ title }: IHeaderProps): React.ReactElement => {
  const [isNavOpen, toggleNav] = useCycle(false, true);
  const [isToggleNavVisible, setToggleNavVisible] = useState(false);

  const handleIsToggleNavVisible = () => {
    if (!isTablet()) {
      if (isToggleNavVisible) {
        toggleNav(1);
        setToggleNavVisible(false);
      }
      return;
    }

    if (!isToggleNavVisible) {
      toggleNav(0);
      setToggleNavVisible(true);
    }
  };

  useEffect(() => {
    handleIsToggleNavVisible();

    window.addEventListener("resize", handleIsToggleNavVisible);
    return () => {
      window.removeEventListener("resize", handleIsToggleNavVisible);
    };
  });

  return (
    <header css={headerStyle}>
      <Logo context="nav" title={title} />

      {isToggleNavVisible && (
        <ToggleNav isNavOpen={isNavOpen} onClick={toggleNav} />
      )}

      <motion.div
        initial="closed"
        css={headerNavStyle}
        variants={isToggleNavVisible && variantsHeader}
        animate={isToggleNavVisible && (isNavOpen ? "open" : "closed")}
      >
        <Nav onClick={toggleNav} />
        <LanguageSelector />
      </motion.div>
    </header>
  );
};

export default Header;
