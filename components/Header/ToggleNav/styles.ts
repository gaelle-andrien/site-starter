export const toggleNavStyle = {
  padding: 0,
  cursor: "pointer",
};

export const toggleNavIconStyle = {
  display: "block",
};
