/** @jsxImportSource @emotion/react */
import { motion } from "framer-motion";
import { useEffect, useState } from "react";

import Path from "./Path";

import { variantsPath, variantsOpacity } from "./variants";
import { toggleNavStyle, toggleNavIconStyle } from "./styles";

const toggleNavLineTop = { d: "M 4.28 20.82 L 20.71 4.17" };
const toggleNavLineMiddle = { d: "M 1.93 12.5 L 23.06 12.5" };
const toggleNavLineBottom = { d: "M 4.28 4.26 L 20.71 20.73" };

const diagonalCrossTop = { d: "M 1.93 4.5 L 23.06 4.5" };
const diagonalCrossBottom = { d: "M 1.93 20.5 L 23.06 20.50" };

interface IToggleNavProps {
  isNavOpen: boolean;
  onClick: () => void;
}

const ToggleNav = ({
  onClick,
  isNavOpen,
}: IToggleNavProps): React.ReactElement => {
  const [variantState, setVariantState] = useState("closed");

  useEffect(() => {
    if (isNavOpen) setVariantState("open");
    if (!isNavOpen) setVariantState("closed");
  }, [isNavOpen]);

  return (
    <motion.button
      initial="closed"
      onClick={onClick}
      css={toggleNavStyle}
      animate={variantState}
    >
      <svg width="30" height="30" viewBox="0 0 25 25" css={toggleNavIconStyle}>
        <Path variants={variantsPath(toggleNavLineTop, diagonalCrossTop)} />

        <Path
          {...toggleNavLineMiddle}
          variants={variantsOpacity}
          transition={{ duration: 0.1 }}
        />

        <Path
          variants={variantsPath(toggleNavLineBottom, diagonalCrossBottom)}
        />
      </svg>
    </motion.button>
  );
};

export default ToggleNav;
