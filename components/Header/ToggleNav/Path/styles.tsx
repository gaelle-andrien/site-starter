const pathStyle = {
  strokeWidth: 3.5,
  stroke: "white",
  fill: "transparent",
  strokeLinecap: "round" as const,
};

export default pathStyle;
