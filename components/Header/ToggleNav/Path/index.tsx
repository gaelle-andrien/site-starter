/** @jsxImportSource @emotion/react */
import { motion, Variants, Transition } from "framer-motion";

import pathStyle from "./styles";

interface IPathProps {
  d?: string;
  variants: Variants;
  transition?: Transition;
}

const Path = ({ d, variants, transition }: IPathProps): React.ReactElement => (
  <motion.path
    d={d}
    css={pathStyle}
    variants={variants}
    transition={transition}
  />
);

export default Path;
