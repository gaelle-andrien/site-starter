import { Variants } from "framer-motion";

interface IPath {
  d: string;
}

export const variantsPath = (pathOpen: IPath, pathClosed: IPath): Variants => ({
  open: pathOpen,
  closed: pathClosed,
});

export const variantsOpacity = {
  open: { opacity: 0 },
  closed: { opacity: 1 },
};
