import { keyframes } from "@emotion/react";

const buttonSpinnerKeyframes = keyframes`
  0% {
    transform: rotate(0turn);
  }
  100% {
    transform: rotate(2turn);
  }
`;

const buttonSpinnerStyles = {
  width: 15,
  height: 15,
  margin: "auto",
  borderRadius: "50%",
  border: "2px solid white",
  borderTopColor: "transparent",
  animation: `${buttonSpinnerKeyframes} 1s ease infinite`,
};

export default buttonSpinnerStyles;
