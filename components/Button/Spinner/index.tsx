/** @jsxImportSource @emotion/react */

import buttonSpinnerStyles from "./styles";

const ButtonSpinner = (): React.ReactElement => (
  <div css={buttonSpinnerStyles} />
);

export default ButtonSpinner;
