import { brandColor } from "../../assets/styles/colors";

const buttonStyle = {
  padding: 10,
  marginTop: 10,
  border: "none",
  outline: "none",
  fontWeight: 500,
  marginBottom: 10,
  letterSpacing: 1,
  color: "inherit",
  cursor: "pointer",
  fontSize: "0.8em",
  alignItems: "center",
  width: "fit-content",
  textDecoration: "none",
  display: "inline-flex",
  backgroundColor: brandColor,
  transition: "all .5s ease-in-out",
  textTransform: "uppercase" as const,

  "&:hover": {
    color: "white",
  },

  "&:disabled": {
    color: "inherit",
    cursor: "default",
    transition: "none",
  },
};

export default buttonStyle;
