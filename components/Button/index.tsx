/** @jsxImportSource @emotion/react */
import { useState } from "react";

import Spinner from "./Spinner";
import Icon, { IButtonIconProps } from "./Icon";
import buttonStyle from "./styles";

import { Position } from "../../utils/types";

interface IButtonProps extends Partial<IButtonIconProps> {
  title: string;
  link?: string;
  isLoading?: boolean;
  isDisabled?: boolean;
  onClick?: () => void;
}

function Button({
  title,
  link,
  icon,
  onClick,
  iconSide,
  isLoading = false,
  isDisabled = false,
}: IButtonProps): React.ReactElement {
  const [isHover, setIsHover] = useState(false);

  let leftSideIcon;
  let rightSideIcon;

  if (icon) {
    const buttonIcon = (
      <Icon icon={icon} iconSide={iconSide} isHover={isHover} />
    );

    if (iconSide === Position.Right) {
      rightSideIcon = buttonIcon;
    } else {
      leftSideIcon = buttonIcon;
    }
  }

  const innerButton = (
    <>
      {leftSideIcon} {title} {rightSideIcon}
    </>
  );

  if (link) {
    return (
      <a href={link} target="_blank" rel="noreferrer" css={buttonStyle}>
        {innerButton}
      </a>
    );
  }

  return (
    <button
      type="button"
      css={buttonStyle}
      onClick={onClick}
      disabled={isDisabled}
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
    >
      {isLoading ? <Spinner /> : innerButton}
    </button>
  );
}

export default Button;
