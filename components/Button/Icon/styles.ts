import styled from "@emotion/styled";

import { Position } from "../../../utils/types";

interface IStyledButtonIcon {
  side?: Position;
}

const StyledButtonIcon = styled.span<IStyledButtonIcon>(
  ({ side = Position.Right }) => ({
    marginLeft: side === Position.Right && 10,
    marginRight: side === Position.Left && 10,
  })
);

export default StyledButtonIcon;
