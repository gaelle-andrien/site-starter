import Icon from "../../Icon";

import StyledButtonIcon from "./styles";

import { Position } from "../../../utils/types";

export interface IButtonIconProps {
  icon: string;
  isHover: boolean;
  iconSide?: Position;
}

const ButtonIcon = ({
  icon,
  isHover,
  iconSide,
}: IButtonIconProps): React.ReactElement => (
  <StyledButtonIcon side={iconSide}>
    <Icon icon={icon} size={20} color={isHover && "white"} />
  </StyledButtonIcon>
);

export default ButtonIcon;
