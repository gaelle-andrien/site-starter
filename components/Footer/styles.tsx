import { layoutMaxWidth } from "../../assets/styles/global";
import { footerBackgroundColor } from "../../assets/styles/colors";

export const footerStyle = {
  width: "100%",
  display: "flex",
  alignSelf: "center",
  flexDirection: "column" as const,
  backgroundColor: footerBackgroundColor,
};

export const footerContentStyle = {
  width: "100%",
  alignSelf: "center",
  maxWidth: layoutMaxWidth,
};
