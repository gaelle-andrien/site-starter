/** @jsxImportSource @emotion/react */

import Container from "../Container";

import { footerStyle, footerContentStyle } from "./styles";

const Footer = (): React.ReactElement => (
  <footer css={footerStyle}>
    <Container hasColumns>
      <div css={footerContentStyle} />
    </Container>
  </footer>
);

export default Footer;
