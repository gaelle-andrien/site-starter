import styled from "@emotion/styled";

import { mq } from "../../assets/styles/mediaqueries";

const size = {
  default: [
    { width: 300, height: 82.5 },
    { width: 400, height: 110 },
    { width: 500, height: 137.5 },
    { width: 600, height: 165 },
  ],
  nav: [
    { width: 75, height: 30 },
    { width: 75, height: 30 },
    { width: 100, height: 40 },
    { width: 100, height: 40 },
  ],
};

interface IStyledlogo {
  context: string;
}

const Styledlogo = styled.img<IStyledlogo>(({ context }) => ({
  display: "block",
  width: size[context][0].width,
  height: size[context][0].height,

  [mq[1]]: {
    width: size[context][1].width,
    height: size[context][1].height,
  },

  [mq[2]]: {
    width: size[context][2].width,
    height: size[context][2].height,
  },

  [mq[3]]: {
    width: size[context][3].width,
    height: size[context][3].height,
  },
}));

export default Styledlogo;
