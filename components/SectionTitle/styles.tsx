import styled from "@emotion/styled";

import { brandColor } from "../../assets/styles/colors";

interface IStyledSectionTitle {
  color?: string;
}

const StyledSectionTitle = styled.h2<IStyledSectionTitle>(
  ({ color = brandColor }) => ({
    color,
  })
);

export default StyledSectionTitle;
