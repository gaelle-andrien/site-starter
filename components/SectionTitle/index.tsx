/** @jsxImportSource @emotion/react */

import StyledSectionTitle from "./styles";

interface ISectionTitleProps {
  title: string;
  color?: string;
}

const SectionTitle = ({
  title,
  color,
}: ISectionTitleProps): React.ReactElement => (
  <StyledSectionTitle color={color}>{title}</StyledSectionTitle>
);

export default SectionTitle;
